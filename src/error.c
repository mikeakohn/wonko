/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "error.h"
#include "tokens.h"
#include "wonko.h"

void error_expected(struct _wonko *wonko, char *expected, char *token)
{
  printf("Error: Expected '%s' but got '%s' on line %d\n", expected, token, wonko->tokens.line);
  wonko->error = ERROR_EXPECTED;
}

void error_unexpected(struct _wonko *wonko, char *token)
{
  printf("Error: Unexpected token '%s' on line %d\n", token, wonko->tokens.line);
  wonko->error = ERROR_UNEXPECTED;
}

void error_undef_var(struct _wonko *wonko, char *token)
{
  printf("Error: Undefined variable '%s' on line %d\n", token, wonko->tokens.line);
  wonko->error = ERROR_UNDEF_VAR;
}

void error_undef_funct(struct _wonko *wonko, char *token)
{
  printf("Error: Undefined function '%s' on line %d\n", token, wonko->tokens.line);
  wonko->error = ERROR_UNDEF_FUNCT;
}

void error_too_many_params(struct _wonko *wonko)
{
  printf("Error: Too many function parameters on line %d\n", wonko->tokens.line);
  wonko->error = ERROR_TOO_MANY_PARAMS;
}

void error_type_mismatch(struct _wonko *wonko, int type_1, int type_2)
{
  printf("Error: Type mismatch of %d and %d on line %d\n", type_1, type_2, wonko->tokens.line);
  wonko->error = ERROR_TYPE_MISMATCH;
}

