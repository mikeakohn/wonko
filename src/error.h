/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _ERROR_H
#define _ERROR_H

#include "wonko.h"

enum
{
  ERROR_OKAY = 0,
  ERROR_UNIMPLEMENTED,
  ERROR_EXPECTED,
  ERROR_UNEXPECTED,
  ERROR_UNDEF_VAR,
  ERROR_UNDEF_FUNCT,
  ERROR_TOO_MANY_PARAMS,
  ERROR_TYPE_MISMATCH,
};

void error_expected(struct _wonko *wonko, char *expected, char *token);
void error_unexpected(struct _wonko *wonko, char *token);
void error_undef_var(struct _wonko *wonko, char *token);
void error_undef_funct(struct _wonko *wonko, char *token);
void error_too_many_params(struct _wonko *wonko);
void error_type_mismatch(struct _wonko *wonko, int type_1, int type_2);

#endif

