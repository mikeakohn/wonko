/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "execute.h"
#include "expression.h"
#include "functions.h"
#include "tokens.h"
#include "var_table.h"

static int execute_set_var(struct _wonko *wonko, char *name)
{
  struct _var var;
  char token[TOKEN_LEN];
  int token_type;
  struct _tokens *tokens = &wonko->tokens;
  struct _var_table *var_table = wonko->var_table;

  token_type = tokens_get(tokens, token, sizeof(token));

  if (IS_NOT_TOKEN(token, '='))
  {
    error_expected(wonko, "=", token);
    return -1;
  }

  memset(&var, 0, sizeof(var));

  if (expression(wonko, &var) == -1) { return -1; }

  if (var.type == VAR_INT)
  {
    var_table_set_int(var_table, name, var.value32);
  }
  else if (var.type == VAR_STRING)
  {
    var_table_set_string(var_table, name, var.string);
  }
  else
  {
    // FIXME - Implement others
    printf("Internal Error at %s:%d\n", __FILE__, __LINE__);
    return -1;
  }

  token_type = tokens_get(tokens, token, sizeof(token));

  if (IS_NOT_TOKEN(token, ';'))
  {
    error_expected(wonko, ";", token);
    return -1;
  }

  return 0;
}

int execute(struct _wonko *wonko)
{
  char token[TOKEN_LEN];
  int token_type;
  struct _var_table *var_table;
  struct _tokens *tokens = &wonko->tokens;

  wonko->var_table = var_table_alloc(32);

  var_table = wonko->var_table;

  while(1)
  {
    token_type = tokens_get(tokens, token, sizeof(token));

    // printf("%d) [%d] %s\n", tokens->line, token_type, token);

    if (token_type == TOKEN_EOF) { break; }

    if (token_type == TOKEN_VARIABLE)
    {
      if (execute_set_var(wonko, token + 1) != 0)
      {
        wonko->error = -1;
        break;
      }
    }
    else if (token_type == TOKEN_KEYWORD)
    {
      function_t function = functions_get(token);

      if (function != NULL)
      {
        if (functions_parse(wonko, function) != 0) { break; }
      }
      else
      {
        error_undef_funct(wonko, token);
        break;
      }
    }
  }

  //var_table_dump(var_table);
  var_table_free(var_table);

  return wonko->error;
}

