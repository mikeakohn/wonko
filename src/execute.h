/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _EXECUTE_H
#define _EXECUTE_H

#include "wonko.h"

int execute(struct _wonko *wonko);

#endif

