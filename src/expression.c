/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "expression.h"
#include "tokens.h"
#include "var_table.h"

enum
{
  PREC_NOT,
  PREC_MUL,
  PREC_ADD,
  PREC_SHIFT,
  PREC_AND,
  PREC_XOR,
  PREC_OR,
  PREC_UNSET
};

enum
{
  OPER_NONE,
  OPER_NOT,
  OPER_MUL,
  OPER_DIV,
  OPER_MOD,
  OPER_ADD,
  OPER_SUB,
  OPER_LEFT_SHIFT,
  OPER_RIGHT_SHIFT,
  OPER_AND,
  OPER_XOR,
  OPER_OR
};

struct _operator
{
  char operation;
  int precedence;
  const char *op;
};

const struct _operator op_table[] =
{
  //{ OPER_NONE, PREC_UNSET, "" },
  { OPER_NOT, PREC_NOT, "~" },
  { OPER_MUL, PREC_MUL, "*" },
  { OPER_DIV, PREC_MUL, "/" },
  { OPER_MOD, PREC_MUL, "%" },
  { OPER_ADD, PREC_ADD, "+" },
  { OPER_SUB, PREC_ADD, "-" },
  { OPER_LEFT_SHIFT, PREC_SHIFT, "<<" },
  { OPER_RIGHT_SHIFT, PREC_SHIFT, ">>" },
  { OPER_AND, PREC_AND, "&" },
  { OPER_XOR, PREC_XOR, "^" },
  { OPER_OR, PREC_OR, "|" },
};

static const struct _operator *expression_get_op(char *token)
{
  const int count = sizeof(op_table) / sizeof(struct _operator);
  int n;

  // NOTE: Might be able to speed this up if token_type comes back
  // with the actual operator and a switch/case was used here.
  for (n = 0; n < count; n++)
  {
    if (token[0] != op_table[n].op[0]) { continue; }
    if (token[1] != op_table[n].op[1]) { continue; }
    if (token[1] == 0) { return &op_table[n]; }
    if (token[2] != op_table[n].op[2]) { continue; }
    if (token[2] == 0) { return &op_table[n]; }
  }

  return NULL;
}

static int expression_exe(struct _wonko *wonko, struct _var *stack, int *stack_ptr, const struct _operator *op)
{
  // FIXME - I think there needs to be a check here that the stack is big
  // enough.
  // FIXME - Move all this to var.c with var_add(var, var) etc and do
  // type checking.

  if (stack[*stack_ptr - 2].type != stack[*stack_ptr - 1].type)
  {
    error_type_mismatch(wonko, stack[*stack_ptr - 2].type, stack[*stack_ptr - 1].type);
    return -1;
  }

  if (stack[*stack_ptr - 2].type == VAR_STRING ||
      stack[*stack_ptr - 1].type == VAR_STRING)
  {
    struct _var_table *var_table = wonko->var_table;
    struct _string_heap *string_heap = &var_table->string_heap;

    // FIXME - Messy.  Should string_heap_remove() be called direct?
    // FIXME - Check return values
    string_heap_start(string_heap);
    string_heap_add(string_heap, stack[*stack_ptr - 2].string);
    string_heap_add(string_heap, stack[*stack_ptr - 1].string);
    char *text = string_heap_end(string_heap);
    //string_heap_remove(stack[*stack_ptr - 1].string);
    //string_heap_remove(stack[*stack_ptr - 2].string);
    var_set_int(&stack[*stack_ptr - 1], string_heap, 0);
    var_set_string(&stack[*stack_ptr - 2], string_heap, text);

    (*stack_ptr)--;

    return 0;
  }

  switch(op->operation)
  {
    case OPER_NOT:
      stack[*stack_ptr - 1].value32 = ~stack[*stack_ptr - 1].value32;
      break;
    case OPER_MUL:
      stack[*stack_ptr - 2].value32 *= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_DIV:
      stack[*stack_ptr - 2].value32 /= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_MOD:
      stack[*stack_ptr - 2].value32 %= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_ADD:
      stack[*stack_ptr - 2].value32 += stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_SUB:
      stack[*stack_ptr - 2].value32 -= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_LEFT_SHIFT:
      stack[*stack_ptr - 2].value32 <<= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_RIGHT_SHIFT:
      stack[*stack_ptr - 2].value32 >>= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_AND:
      stack[*stack_ptr - 2].value32 &= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_XOR:
      stack[*stack_ptr - 2].value32 ^= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    case OPER_OR:
      stack[*stack_ptr - 2].value32 |= stack[*stack_ptr - 1].value32;
      (*stack_ptr)--;
      break;
    default:
      printf("Internal Error: %s:%d\n", __FILE__, __LINE__);
      break;
  }

  return 0;
}

int expression(struct _wonko *wonko, struct _var *var)
{
  char token[TOKEN_LEN];
  int token_type;
  struct _var stack[3];
  int stack_ptr = 1;
  //int operator = OPER_NONE;
  const struct _operator *op = NULL;
  struct _tokens *tokens = &wonko->tokens;
  struct _var_table *var_table = wonko->var_table;
  struct _string_heap *string_heap = &var_table->string_heap;

  memcpy(&stack[0], var, sizeof(struct _var));

  while(1)
  {
    token_type = tokens_get(tokens, token, sizeof(token));

    // FIXME - Need to add paranthesis support
    if (IS_TOKEN(token, ')'))
    {
      tokens_pushback(tokens, token, token_type);
      break;
    }

    if (token_type == TOKEN_NUMBER)
    {
      var_set_int(&stack[stack_ptr], string_heap, atoi(token));
      stack_ptr++;
    }
    else if (token_type == TOKEN_FLOAT)
    {
      var_set_float(&stack[stack_ptr], string_heap, atof(token));
      stack_ptr++;
    }
    else if (token_type == TOKEN_STRING)
    {
      var_set_string(&stack[stack_ptr], string_heap, token);
      stack_ptr++;
    }
    else if (token_type == TOKEN_VARIABLE)
    {
      struct _var *var = var_table_get(var_table, token + 1);

      // Check if variable is defined
      if (var == NULL)
      {
        error_undef_var(wonko, token);
        return -1;
      }

      // Check for overflow of stack.
      if (stack_ptr == 3)
      {
        error_unexpected(wonko, token);
        return -1;
      }

      // FIXME: Ouch.
      stack[stack_ptr].value64 = var->value64;
      stack[stack_ptr].type = var->type;
      stack_ptr++;
    }
    else if (IS_TOKEN(token, ';') || IS_TOKEN(token, ','))
    {
      tokens_pushback(tokens, token, token_type);
      break;
    }
    else
    {
      if (token_type != TOKEN_SYMBOL)
      {
        error_unexpected(wonko, token);
        return -1;
      }

      const struct _operator *new_op = expression_get_op(token);

      if (new_op == NULL)
      {
        error_unexpected(wonko, token);
        return -1;
      }

      if (op == NULL)
      {
        op = new_op;
      }
      else
      {
        if (new_op->precedence < op->precedence)
        {
          if (stack_ptr == 0)
          {
            error_unexpected(wonko, token);
            return -1;
          }

          tokens_pushback(tokens, token, token_type);

          if (expression(wonko, &stack[stack_ptr - 1]) == -1)
          {
            return -1;
          }
        }
        else
        {
          if (expression_exe(wonko, stack, &stack_ptr, op) != 0) { return -1; }
          op = new_op;
        }
      }
    }

    if (IS_TOKEN(token, ';'))
    {
      tokens_pushback(tokens, token, token_type);
      break;
    }
  }

  if (op != NULL)
  {
    if (expression_exe(wonko, stack, &stack_ptr, op) != 0) { return -1; }
  }

  if (stack_ptr < 1)
  {
    printf("Internal Error: %s:%d\n", __FILE__, __LINE__);
    return -1;
  }

  memcpy(var, &stack[stack_ptr - 1], sizeof(struct _var));

  return 0;
}

