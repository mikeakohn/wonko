/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _EXPRESSION_H
#define _EXPRESSION_H

#include <stdint.h>

#include "tokens.h"
#include "var_table.h"
#include "wonko.h"

int expression(struct _wonko *wonko, struct _var *var);

#endif

