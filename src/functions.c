/**
 *  Wonko
 *  Authors: Michael Kohn
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "expression.h"
#include "functions.h"
#include "functions_debug.h"
#include "functions_output.h"
#include "functions_var.h"
#include "tokens.h"

struct _functions functions[] =
{
  // Vars
  { "int", functions_int, 1 },
  { "str", functions_str, 1 },
  { "float", functions_float, 1 },
  // Output
  { "print", functions_print, 1 },
  { "println", functions_println, 1 },
  // Debug
  { "dump_vars", functions_dump_vars, 0 },
  { NULL, NULL },
};

function_t functions_get(char *name)
{
  int n = 0;

  while(functions[n].name != NULL)
  {
    if (strcmp(functions[n].name, name) == 0)
    {
      return functions[n].function;
    }

    n++;
  }

  return NULL;
}

int functions_parse(struct _wonko *wonko, function_t function)
{
  struct _var params[PARAM_COUNT_MAX];
  int param_count = 0;
  struct _var ret_value;
  char token[TOKEN_LEN];
  int token_type;
  struct _tokens *tokens = &wonko->tokens;

  token_type = tokens_get(tokens, token, sizeof(token));

  if (IS_NOT_TOKEN(token, '('))
  {
    error_expected(wonko, ";", token);
    return -1;
  }

  token_type = tokens_get(tokens, token, sizeof(token));

  if (IS_NOT_TOKEN(token, ')'))
  {
    tokens_pushback(tokens, token, token_type);

    while(1)
    {
      if (expression(wonko, &params[param_count]) == -1) { return -1; }

      param_count++;

      if (param_count > PARAM_COUNT_MAX)
      {
        error_too_many_params(wonko);
        return -1;
      }

      token_type = tokens_get(tokens, token, sizeof(token));

      if (IS_TOKEN(token, ')')) { break; }

      if (IS_NOT_TOKEN(token, ','))
      {
        error_unexpected(wonko, token);
        return -1;
      }
    }
  }

  token_type = tokens_get(tokens, token, sizeof(token));

  if (IS_NOT_TOKEN(token, ';'))
  {
    error_expected(wonko, ";", token);
    return -1;
  }

  if (function(wonko, &ret_value, params, param_count) != 0) { return -1; }

  return 0;
}

