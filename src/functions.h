/**
 *  Wonko
 *  Authors: Michael Kohn
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <stdint.h>

#include "wonko.h"

#define PARAM_COUNT_MAX 6

typedef int (*function_t)(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count);

struct _functions
{
  char *name;
  function_t function;
  uint8_t params;
};

function_t functions_get(char *name);
int functions_parse(struct _wonko *wonko, function_t function);

#endif

