/**
 *  Wonko
 *  Authors: Michael Kohn
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "functions_output.h"
#include "var_table.h"

int functions_dump_vars(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count)
{
  var_table_dump(wonko->var_table);

  return 0;
}

