/**
 *  Wonko
 *  Authors: Michael Kohn
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "expression.h"
#include "functions_output.h"
#include "var_table.h"

int functions_print(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count)
{
  switch (params[0].type)
  {
    case VAR_INT:
      printf("%d", params[0].value32);
      break;
    case VAR_LONG:
      printf("%ld", params[0].value64);
      break;
    case VAR_FLOAT:
      printf("%f", params[0].value_f);
      break;
    case VAR_DOUBLE:
      printf("%f", params[0].value_d);
      break;
    case VAR_STRING:
      printf("%s", params[0].string);
      break;
    default:
      printf("Error: Unimplemented on line %d\n", wonko->tokens.line);
      return ERROR_UNIMPLEMENTED;
  }

  return 0;
}

int functions_println(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count)
{
  if (functions_print(wonko, ret_value, params, param_count) != 0) { return -1; }

  putchar('\n');

  return 0;
}

