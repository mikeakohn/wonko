/**
 *  Wonko
 *  Authors: Michael Kohn
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _FUNCTIONS_OUTPUT_H
#define _FUNCTIONS_OUTPUT_H

#include <stdint.h>

#include "wonko.h"

int functions_print(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count);
int functions_println(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count);

#endif

