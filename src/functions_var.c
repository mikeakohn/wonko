/**
 *  Wonko
 *  Authors: Michael Kohn
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"
#include "functions_var.h"

int functions_int(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count)
{
  switch (params[0].type)
  {
    case VAR_INT:
      //params[0].value32 = params[0].value32;
      break;
    case VAR_LONG:
      params[0].value32 = params[0].value64;
      break;
    case VAR_FLOAT:
      params[0].value32 = (int)params[0].value_f;
      break;
    case VAR_DOUBLE:
      params[0].value32 = (int)params[0].value_d;
      break;
    case VAR_STRING:
      if (params[0].string == NULL)
      {
        params[0].value32 = 0;
      }
      else
      {
        params[0].value32 = atoi(params[0].string);
      }
      break;
    default:
      printf("Error: Unimplemented on line %d\n", wonko->tokens.line);
      return ERROR_UNIMPLEMENTED;
  }

  params[0].type = VAR_INT;

  return 0;
}

int functions_str(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count)
{
  char text[128];

  switch (params[0].type)
  {
    case VAR_INT:
      snprintf(text, sizeof(text), "%d", params[0].value32);
      break;
    case VAR_LONG:
      snprintf(text, sizeof(text), "%ld", params[0].value64);
      break;
    case VAR_FLOAT:
      snprintf(text, sizeof(text), "%f", params[0].value_f);
      break;
    case VAR_DOUBLE:
      snprintf(text, sizeof(text), "%f", params[0].value_d);
      break;
    case VAR_STRING:
      return 0;
    default:
      printf("Error: Unimplemented on line %d\n", wonko->tokens.line);
      return ERROR_UNIMPLEMENTED;
  }

  //params[0].string = var_make_string(text);
  params[0].type = VAR_STRING;

  return 0;
}

int functions_float(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count)
{
  switch (params[0].type)
  {
    case VAR_INT:
      params[0].value_f = (float)params[0].value32;
      break;
    case VAR_LONG:
      params[0].value_f = (float)params[0].value64;
      break;
    case VAR_FLOAT:
      //params[0].value_f = (int)params[0].value_f;
      break;
    case VAR_DOUBLE:
      params[0].value_f = (int)params[0].value_d;
      break;
    case VAR_STRING:
      if (params[0].string == NULL)
      {
        params[0].value32 = 0;
      }
      else
      {
        params[0].value32 = atof(params[0].string);
      }
      break;
    default:
      printf("Error: Unimplemented on line %d\n", wonko->tokens.line);
      return ERROR_UNIMPLEMENTED;
  }

  params[0].type = VAR_FLOAT;

  return 0;
}

