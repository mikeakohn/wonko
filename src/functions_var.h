/**
 *  Wonko
 *  Authors: Michael Kohn
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _FUNCTIONS_VAR_H
#define _FUNCTIONS_VAR_H

#include <stdint.h>

#include "wonko.h"

int functions_int(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count);
int functions_str(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count);
int functions_float(struct _wonko *wonko, struct _var *ret_value, struct _var *params, int param_count);

#endif

