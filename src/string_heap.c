/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "string_heap.h"

int string_heap_reset(struct _string_heap *string_heap)
{
  string_heap->ptr = 0;
  string_heap->string_count = 0;

  return 0;
}

int string_heap_start(struct _string_heap *string_heap)
{
  string_heap->start_ptr = string_heap->ptr;

  return 0;
}

int string_heap_add(struct _string_heap *string_heap, char *text)
{
  while(*text != 0)
  {
    if (string_heap->ptr >= STRING_HEAP_SIZE) { return -1; }

    string_heap->buffer[string_heap->ptr++] = *text;
    text++;
  }

  return 0;
}

char *string_heap_end(struct _string_heap *string_heap)
{
  if (string_heap->ptr >= STRING_HEAP_SIZE) { return NULL; }

  string_heap->buffer[string_heap->ptr++] = 0;

  return string_heap->buffer + string_heap->start_ptr;
}

int string_heap_remove(struct _string_heap *string_heap, char *text)
{
  string_heap->string_count--;

  if (string_heap->string_count == 0)
  {
    return string_heap_reset(string_heap);
  }

  return 0;
}

int string_heap_optimize(struct _string_heap *string_heap)
{
  if (string_heap->string_count == 0)
  {
    return string_heap_reset(string_heap);
  }

#if 0
  if (string_heap->string_count == 1 && string_heap->ptr != 0)
  {
    int len = string_heap->ptr - string_heap->start_ptr;

    memmove(string_heap->buffer,
            string_heap->buffer + string_heap->start_ptr,
            len);

    string_heap->start_ptr = 0;
    string_heap->ptr -= len;
  }
#endif

  return 0;
}

