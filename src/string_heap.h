/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _STRING_HEAP_H
#define _STRING_HEAP_H

#include <stdint.h>

#define STRING_HEAP_SIZE 8192

// FIXME: Needs a reference counter.

struct _string_heap
{
  int ptr;
  int start_ptr;
  int len;
  int string_count;
  char buffer[STRING_HEAP_SIZE];
};

int string_heap_reset(struct _string_heap *string_heap);
int string_heap_start(struct _string_heap *string_heap);
int string_heap_add(struct _string_heap *string_heap, char *text);
char *string_heap_end(struct _string_heap *string_heap);
int string_heap_remove(struct _string_heap *string_heap, char *text);
int string_heap_optimize(struct _string_heap *string_heap);

#endif

