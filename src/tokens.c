/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokens.h"

static void tokens_init(struct _tokens *tokens)
{
  tokens->line = 1;
}

static void tokens_push_char(struct _tokens *tokens, int ch)
{
  if (tokens->in != NULL)
  {
    ungetc(ch, tokens->in);
  }
  else
  {
    tokens->buffer_ptr--;
  }
}

void tokens_init_file(struct _tokens *tokens, FILE *in)
{
  memset(tokens, 0, sizeof(struct _tokens));

  tokens_init(tokens);
  tokens->in = in;
}

void tokens_init_buffer(struct _tokens *tokens, char *buffer)
{
  memset(tokens, 0, sizeof(struct _tokens));

  tokens_init(tokens);
  tokens->buffer = buffer;
}

int tokens_get(struct _tokens *tokens, char *token, int length)
{
  int token_type = TOKEN_INVALID;
  int ptr = 0;
  int ch;

  if (tokens->pushback_count != 0)
  {
    tokens->pushback_count--;
    strcpy(token, tokens->pushback[tokens->pushback_count].token);
    return tokens->pushback[tokens->pushback_count].token_type;
  }

  while(1)
  {
    // Read next token from stream
    if (tokens->in != NULL)
    {
      ch = getc(tokens->in);
    }
    else
    {
      ch = tokens->buffer[tokens->buffer_ptr];
      if (ch == 0) { ch = EOF; }
      else { tokens->buffer_ptr++; }
    }

    if (ch == EOF) { break; }

    // Support for obsolete 1970's operating systems.
    if (ch == '\r') { continue; }

    // For end of line or space..
    if (ch == '\n' || (ch == ' ' && token_type != TOKEN_STRING))
    {
      if (ch == '\n') tokens->line++;
      if (ptr == 0) { continue; }
      break;
    }

    // Read in string until a closing quote is hit
    if (token_type == TOKEN_STRING)
    {
      if (ch == '\"') { break; }
      token[ptr++] = ch;
      continue;
    }

    // Start of a QUOTED string
    if (ch == '\"' && ptr == 0)
    {
      token_type = TOKEN_STRING;
      continue;
    }

    // Start of a variable
    if (ch == '$' && ptr == 0)
    {
      token_type = TOKEN_VARIABLE;
      token[ptr++] = ch;
      continue;
    }

    // This is a symbol: !,",#,$,%,&,',(,),*,+,,,-,.,/
    if ((ch >= 33 && ch <= 47) || (ch >= 58 && ch <= 64))
    {
      if (ptr == 0)
      {
        token[ptr++] = ch;
        token_type = TOKEN_SYMBOL;
        break;
      }
      else
      {
        tokens_push_char(tokens, ch);
        break;
      }

      continue;
    }

    if (ch >= '0' && ch <= '9' && ptr == 0)
    {
      token_type = TOKEN_NUMBER;
    }

    if (token_type == TOKEN_FLOAT)
    {
      if (ch < '0' || ch > '9') { token_type = TOKEN_ERROR; }
    }

    if (token_type == TOKEN_NUMBER)
    {
      if (ch == '.') { token_type = TOKEN_FLOAT; }
      if (ch < '0' || ch > '9') { token_type = TOKEN_ERROR; }
    }

    token[ptr++] = ch;
  }

  token[ptr] = 0;
  if (ptr == 0) { return TOKEN_EOF; }

  if ((token[0] >= 'a' && token[0] <= 'z') ||
      (token[0] >= 'A' && token[0] <= 'Z'))
  {
    // FIXME: Do this correctly.
    if (token_type != TOKEN_STRING)
    {
      return TOKEN_KEYWORD;
    }
  }

  return token_type;
}

int tokens_pushback(struct _tokens *tokens, char *token, int token_type)
{
  if (tokens->pushback_count >= PUSHBACK_LEN) { return -1; }

  tokens->pushback[tokens->pushback_count].token_type = token_type;
  strcpy(tokens->pushback[tokens->pushback_count].token, token);

  tokens->pushback_count++;

  return 0;
}


