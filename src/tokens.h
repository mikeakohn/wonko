/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _TOKENS_H
#define _TOKENS_H

#define TOKEN_LEN 1024
#define PUSHBACK_LEN 4

#define IS_TOKEN(t,a) (t[0]==a && t[1]==0)
#define IS_NOT_TOKEN(t,a) (t[0]!=a || t[1]!=0)

enum
{
  TOKEN_INVALID,
  TOKEN_ERROR,
  TOKEN_EOF,
  TOKEN_NUMBER,
  TOKEN_FLOAT,
  TOKEN_STRING,
  TOKEN_KEYWORD,
  TOKEN_VARIABLE,
  TOKEN_SYMBOL,
};

struct _token
{
  int token_type;
  int length;
  char token[TOKEN_LEN];
};

struct _tokens
{
  FILE *in;
  char *buffer;
  int buffer_ptr;
  int line;
  int pushback_count;
  struct _token pushback[PUSHBACK_LEN];
};

void tokens_init_file(struct _tokens *tokens, FILE *in);
void tokens_init_buffer(struct _tokens *tokens, char *buffer);
int tokens_get(struct _tokens *tokens, char *buffer, int length);
int tokens_pushback(struct _tokens *tokens, char *token, int token_type);

#endif


