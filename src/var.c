/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "string_heap.h"
#include "var.h"

int var_unset(struct _var *var, struct _string_heap *string_heap)
{
  if (var->type == VAR_STRING)
  {
    string_heap_remove(string_heap, var->string);
    var->string = NULL;
  }

  var->type = VAR_INT;
  var->value32 = 0;

  return 0;
}


int var_set_int(struct _var *var, struct _string_heap *string_heap, uint32_t value)
{
  var_unset(var, string_heap);

  var->value32 = value;
  var->type = VAR_INT;

  return 0;
}

int var_set_long(struct _var *var, struct _string_heap *string_heap, uint64_t value)
{
  var_unset(var, string_heap);

  var->value64 = value;
  var->type = VAR_LONG;

  return 0;
}

int var_set_float(struct _var *var, struct _string_heap *string_heap, float value)
{
  var_unset(var, string_heap);

  var->value_f = value;
  var->type = VAR_FLOAT;

  return 0;
}

int var_set_string(struct _var *var, struct _string_heap *string_heap, char *text)
{
  var_unset(var, string_heap);

  string_heap_start(string_heap);
  string_heap_add(string_heap, text);
  var->string = string_heap_end(string_heap);

  if (var->string == NULL) { return -1; }

  var->type = VAR_STRING;

  return 0;
}

