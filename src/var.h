/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _VAR_H
#define _VAR_H

#include <stdint.h>

enum
{
  VAR_INT,
  VAR_LONG,
  VAR_FLOAT,
  VAR_DOUBLE,
  VAR_STRING,
  VAR_ARRAY,
  VAR_LIST,
  VAR_DICT,
};

struct _var
{
  char *name;
  union
  {
    uint64_t value64;
    uint32_t value32;
    float value_f;
    double value_d;
    char *string;
  };
  int type;
};

int var_unset(struct _var *var, struct _string_heap *string_heap);
int var_set_int(struct _var *var, struct _string_heap *string_heap, uint32_t value);
int var_set_long(struct _var *var, struct _string_heap *string_heap, uint64_t value);
int var_set_float(struct _var *var, struct _string_heap *string_heap, float value);
int var_set_string(struct _var *var, struct _string_heap *string_heap, char *text);

#endif

