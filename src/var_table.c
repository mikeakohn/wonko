/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "var_table.h"

struct _var_table *var_table_alloc(int length)
{
  int size = sizeof(struct _var_table) + (sizeof(struct _var) * length);
  struct _var_table *var_table = (struct _var_table *)malloc(size);
  memset(var_table, 0, size);

  var_table->total_length = length;

  return var_table;
}

void var_table_free(struct _var_table *var_table)
{
  free(var_table);
}

struct _var *var_table_get(struct _var_table *var_table, char *name)
{
  int n;

  // FIXME - Replace with hash table later.
  for (n = 0; n < var_table->length; n++)
  {
    if (strcmp(var_table->vars[n].name, name) == 0)
    {
      return &var_table->vars[n];
    }
  }

  return NULL;
}

struct _var *var_table_add(struct _var_table *var_table, char *name)
{
  // If table is full, return NULL.
  if (var_table->length == var_table->total_length) { return NULL; }

  int name_len = strlen(name) + 1;

  // Only support 4k of names for now.
  if (var_table->names_ptr + name_len >= NAMES_HEAP_LEN)
  {
    return NULL;
  }

  int index = var_table->length++;

  // Copy string into names heap
  strcpy(var_table->names_heap + var_table->names_ptr, name);

  // Set up variable
  var_table->vars[index].name = var_table->names_heap + var_table->names_ptr;
  var_table->vars[index].type = 0;
  //var_table->vars[index].value64 = 0;

  var_table->names_ptr += name_len;

  return var_table->vars + index;
}

struct _var *var_table_get_and_add(struct _var_table *var_table, char *name)
{
  struct _var *var = var_table_get(var_table, name);

  if (var == NULL)
  {
    var = var_table_add(var_table, name);
    if (var == NULL) { return NULL; }
  }
  else
  {
    var_unset(var, &var_table->string_heap);
  }

  return var;
}

int var_table_set_int(struct _var_table *var_table, char *name, uint32_t value)
{
  struct _var *var;

  var = var_table_get_and_add(var_table, name);

  if (var == NULL) { return -1; };

  return var_set_int(var, &var_table->string_heap, value);
}

int var_table_set_long(struct _var_table *var_table, char *name, uint64_t value)
{
  struct _var *var;

  var = var_table_get_and_add(var_table, name);

  if (var == NULL) { return -1; };

  return var_set_long(var, &var_table->string_heap, value);
}

int var_table_set_string(struct _var_table *var_table, char *name, char *text)
{
  struct _var *var;

  var = var_table_get_and_add(var_table, name);

  if (var == NULL) { return -1; };

  return var_set_string(var, &var_table->string_heap, text);
}

void var_table_dump(struct _var_table *var_table)
{
  int n;

  printf("Variable Table\n");
  printf("--------------\n");

  for (n = 0; n < var_table->length; n++)
  {
    printf("%-20s %ld %d\n", var_table->vars[n].name,
                            var_table->vars[n].value64,
                            var_table->vars[n].type);
  }
}



