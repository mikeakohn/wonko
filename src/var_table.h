/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _VAR_TABLE_H
#define _VAR_TABLE_H

#include <stdint.h>

#include "string_heap.h"
#include "var.h"

// All variable names length added up can't exceed 4k right now
#define NAMES_HEAP_LEN 4096

struct _var_table
{
  int length;
  int total_length;
  int names_ptr;
  struct _string_heap string_heap;
  char names_heap[NAMES_HEAP_LEN];
  struct _var vars[0];
};

struct _var_table *var_table_alloc(int length);
void var_table_free(struct _var_table *var_table);
struct _var *var_table_get(struct _var_table *var_table, char *name);
struct _var *var_table_get_and_add(struct _var_table *var_table, char *name);
struct _var *var_table_add(struct _var_table *var_table, char *name);
int var_table_set_int(struct _var_table *var_table, char *name, uint32_t value);
int var_table_set_long(struct _var_table *var_table, char *name, uint64_t value);
int var_table_set_string(struct _var_table *var_table, char *name, char *text);
void var_table_dump(struct _var_table *var_table);

#endif

