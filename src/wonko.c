#include <stdio.h>
#include <stdlib.h>

#include "execute.h"
#include "tokens.h"
#include "wonko.h"

static int wonko_init(struct _wonko *wonko)
{
  wonko->error = 0;

  return 0;
}

int wonko_init_file(struct _wonko *wonko, FILE *in)
{
  wonko_init(wonko);
  tokens_init_file(&wonko->tokens, in);

  return 0;
}

int wonko_init_buffer(struct _wonko *wonko, char *buffer)
{
  wonko_init(wonko);
  tokens_init_buffer(&wonko->tokens, buffer);

  return 0;
}

int main(int argc, char *argv[])
{
  FILE *in;
  struct _wonko wonko;

  if (argc != 2)
  {
    printf("Usage: wonko <file.ns>\n");
    exit(0);
  }

  in = fopen(argv[1], "rb");
  if (in == NULL)
  {
    printf("Cannot open %s\n", argv[1]);
    exit(1);
  }

  wonko_init_file(&wonko, in);

  execute(&wonko);

  fclose(in);

  return 0;
}

