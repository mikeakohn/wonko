/**
 *  Wonko
 *  Authors:
 *    Email: mike@mikekohn.net
 *      Web: http://www.mikekohn.net/
 *  License: BSD
 *
 * Copyright 2017
 *
 */

#ifndef _WONKO_H
#define _WONKO_H

#include "tokens.h"
#include "var_table.h"

struct _wonko
{
  struct _tokens tokens;
  struct _var_table *var_table;
  int error;
};

#endif

